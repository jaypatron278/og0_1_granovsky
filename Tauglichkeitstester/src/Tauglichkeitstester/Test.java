package Tauglichkeitstester;

public abstract class Test {

	protected boolean aktiv;
	protected String ergebnis;
	
	protected abstract void warten();
	
	public abstract String getErgebnis();
	
	public abstract boolean isAktiv();
	
	public abstract void starten();
	
	public abstract void stoppen();
	
	public abstract void zeigeHilfe();
	
	
}
