package Tauglichkeitstester;
import java.math.BigInteger;

public class Stoppuhr {
	
	private final long nanoSecondsPerMillisecond = 1000000;
    private final long nanoSecondsPerSecond = 1000000000;
    private final long nanoSecondsPerMinute = 60000000000L;
    private final long nanoSecondsPerHour = 3600000000000L;

    private long stopWatchStartTime = 0;
    private long stopWatchStopTime = 0;
    private boolean stopWatchRunning = false;


    public void start() {
        this.stopWatchStartTime = System.nanoTime();
        this.stopWatchRunning = true;
    }


    public void stop() {
        this.stopWatchStopTime = System.nanoTime();
        this.stopWatchRunning = false;
    }


    public long getElapsedMilliseconds() {
        long zeit1;

        if (stopWatchRunning)
            zeit1 = (System.nanoTime() - stopWatchStartTime);
        else
            zeit1 = (stopWatchStopTime - stopWatchStartTime);

        return zeit1 / nanoSecondsPerMillisecond;
    }


    public long getElapsedSeconds() {
        long elapsedTime;

        if (stopWatchRunning)
            elapsedTime = (System.nanoTime() - stopWatchStartTime);
        else
            elapsedTime = (stopWatchStopTime - stopWatchStartTime);

        return elapsedTime / nanoSecondsPerSecond;
    }


    public long getElapsedMinutes() {
        long elapsedTime;
        if (stopWatchRunning)
            elapsedTime = (System.nanoTime() - stopWatchStartTime);
        else
            elapsedTime = (stopWatchStopTime - stopWatchStartTime);

        return elapsedTime / nanoSecondsPerMinute;
    }


    public long getElapsedHours() {
        long elapsedTime;
        if (stopWatchRunning)
            elapsedTime = (System.nanoTime() - stopWatchStartTime);
        else
            elapsedTime = (stopWatchStopTime - stopWatchStartTime);

        return elapsedTime / nanoSecondsPerHour;
    }


}

 class stoppuhr {

    public static void main(String[] args) {

        Stoppuhr stoppuhrneu = new Stoppuhr();
        stoppuhrneu.start();
        Fibonacci(45);
        stoppuhrneu.stop();


        System.out.println("Milisekunden: "
                + stoppuhrneu.getElapsedMilliseconds());

        System.out.println("in  Sekunden: "
                + stoppuhrneu.getElapsedSeconds());

        System.out.println("in Minuten: "
                + stoppuhrneu.getElapsedMinutes());

        System.out.println("in Stunden: "
                + stoppuhrneu.getElapsedHours());

    }

    private static BigInteger Fibonacci(int n) {
        if (n < 2)
            return BigInteger.ONE;
        else
            return Fibonacci(n - 1).add(Fibonacci(n - 2)); 
    }

}
