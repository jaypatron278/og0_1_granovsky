package de.futurehome.tanksimulator;

import java.awt.BorderLayout;

import java.awt.*;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;

import javax.swing.JSlider;

@SuppressWarnings("serial")
public class TankSimulator extends Frame {
	
	public Tank myTank;
	
	private Label lblUeberschrift = new Label("Tank-Simulator");
	public  Label lblFuellstand = new Label("     ");
	public 	Label lblProzentstand = new Label("   ");
	public 	Label lblTankHoch = new Label("     ");
	
	public Button btnBeenden = new Button("Beenden");
	public Button btnEinfuellen = new Button("Einf�llen");
	public Button btnVerbrauchen = new Button("Verbrauchen");
	public Button btnZurucksetzen = new Button("Zur�cksetzen");
	
	//Slider einf�gen
	JSlider mySlider = new JSlider();

	
	
	private Panel pnlNorth = new Panel();
	private Panel pnlCenter = new Panel(new FlowLayout());
	private Panel pnlSouth = new Panel(new GridLayout(1, 0));
	private Panel pnlRight = new Panel(new GridLayout(1, 0));

	private MyActionListener myActionListener = new MyActionListener(this);

	public TankSimulator() {
		super("Tank-Simulator");
		
		myTank = new Tank(0);
		
		//Slider Methoden einf�gen 
		
		mySlider.setMinimum(1);
		mySlider.setMaximum(4);
		mySlider.setMajorTickSpacing(1);
		mySlider.setMinorTickSpacing(1);
		mySlider.createStandardLabels(1);
		mySlider.setPaintTicks(true);
		mySlider.setPaintLabels(true);
		mySlider.setValue(1);

		
		
		//Style
		this.lblUeberschrift.setFont(new Font("", Font.BOLD, 16));
		this.pnlNorth.add(this.lblUeberschrift);
		this.pnlCenter.add(this.lblFuellstand);
		this.pnlRight.add(this.lblProzentstand);
		this.pnlRight.add(this.lblTankHoch);
		this.pnlSouth.add(this.btnEinfuellen);
		this.pnlSouth.add(this.btnVerbrauchen);
		this.pnlSouth.add(this.btnZurucksetzen);
		this.pnlSouth.add(this.btnBeenden);
		this.pnlCenter.add(this.mySlider);
		this.add(this.pnlNorth, BorderLayout.NORTH);
		this.add(this.pnlCenter, BorderLayout.CENTER);
		this.add(this.pnlSouth, BorderLayout.SOUTH);
		this.add(this.pnlRight, BorderLayout.EAST);
		this.pack();
		this.setVisible(true);
		
		// Ereignissteuerung
		this.btnEinfuellen.addActionListener(myActionListener);
		this.btnVerbrauchen.addActionListener(myActionListener);
		this.btnBeenden.addActionListener(myActionListener);
		this.btnZurucksetzen.addActionListener(myActionListener);
	}
	


	public static void main(String argv[]) {
		TankSimulator f = new TankSimulator();
	}
}