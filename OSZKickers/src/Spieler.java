
public class Spieler extends Person{
	
	private String spielposition;
	private int trikotNummer;
	
	
	Spieler(){
		
	}


	public String getSpielposition() {
		return spielposition;
	}


	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}


	public int getTrikotNummer() {
		return trikotNummer;
	}


	public void setTrikotNummer(int trikotNummer) {
		this.trikotNummer = trikotNummer;
	}
	
	
	

}
