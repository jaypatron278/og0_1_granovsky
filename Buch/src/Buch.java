
public class Buch implements Comparable<Buch>{
	
	private String autor;
	private String buchName;
	private String isbn;
	
	public Buch(String autor, String buchName, String isbn) {
		this.autor = autor;
		this.buchName = buchName;
		this.isbn = isbn;
	}

	
	public String getAutor() {
		return autor;
	}



	public void setAutor(String autor) {
		this.autor = autor;
	}



	public String getBuchName() {
		return buchName;
	}



	public void setBuchName(String buchName) {
		this.buchName = buchName;
	}



	public String getIsbn() {
		return isbn;
	}



	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	

	public boolean equals(Buch b){
		return b.getIsbn().equals(this.isbn);
	}
	
	public String toString(){
		return "[  "+this.isbn + ", "+ "Buchname: "+this.buchName + " ]";
	}
	
	public int compareTo(Buch b){
		return this.isbn.compareTo(b.getIsbn());
		
	}


	
}
