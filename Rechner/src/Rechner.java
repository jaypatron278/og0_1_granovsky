import java.util.Scanner;

public class Rechner {
    public static void main(String[] args) {
        while (true)
        {
            String eingabe1;
            int zahl1, zahl2;

            try {
            System.out.println("Geben Sie eine ganze Zahl ein (x zum beenden): ");
            eingabe1 = new Scanner(System.in).nextLine();

            if (eingabe1.equalsIgnoreCase("x"))
                break;

            zahl1 = Integer.parseInt(eingabe1);

            System.out.println("Geben Sie eine ganze Zahl ein: ");
            zahl2 = Integer.parseInt(new Scanner(System.in).nextLine());

            
                int zahl3 = zahl1+zahl2;

                System.out.printf("%d + %d = %d%n", zahl1, zahl2, zahl1 + zahl2);
                System.out.printf("%d - %d = %d%n", zahl1, zahl2, zahl1 - zahl2);
                System.out.printf("%d * %d = %d%n", zahl1, zahl2, zahl1 * zahl2);
                System.out.printf("%d / %d = %d%n", zahl1, zahl2, zahl1 / zahl2);
            }catch(ArithmeticException e) {
            	System.out.println("Die angegebenen Daten sind zu �berpr�fen");
            	
            }catch(NumberFormatException exception) {
            	System.out.println("Die Eingabe ist ung�ltig");
            	
            }
        
        }
    }
}