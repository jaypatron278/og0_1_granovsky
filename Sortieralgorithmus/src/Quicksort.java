
public class Quicksort {
	
	int zahler = 0; 

	public int listenTeilung(int liste[], int erstes, int letztes) {
		int pivot = liste[(erstes+ letztes) /2];
		
		int positionLinks = erstes; 
		int positionRechts = letztes; 
		
		while(positionLinks <= positionRechts) {
			while(liste[positionLinks]<pivot) {
				positionLinks++;
			}
			
			while(liste[positionRechts]> pivot) {
				positionRechts--;
			}
			
			if(positionLinks <= positionRechts) {
				int tempo = liste[positionLinks];
				liste[positionLinks] = liste[positionRechts];
				liste[positionRechts] = tempo;
				positionLinks++;
				positionRechts--;
			}
			
			/*for(int i = 0; i< liste.length; i++) {
				System.out.print(liste[i] + " ,");
			}*/
		}
		
		return positionLinks;
	}
	
	public void quickSort(int liste[], int erstes, int letztes) {
		int index = listenTeilung(liste, erstes,letztes);
		
		if(erstes<index-1) {
			quickSort(liste,erstes,index-1);
		}
		
		if(index< letztes) {
			quickSort(liste,index,letztes);
		}
	}
	
}
