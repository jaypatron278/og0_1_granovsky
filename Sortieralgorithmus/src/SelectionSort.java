
public class SelectionSort {
	
	public static void tausch(int[] array, int links, int rechts) {
		
		int tempo = array[links];
		array[links] = array[rechts];
		array[rechts] = tempo;
		
	}
	
	public static void selectionSort(int[] liste) {
		
		int min; 
		
		for(int i= 0; i<liste.length-1; i++) {
			min = i;
			
			for(int j= i+1; j<liste.length; j++) {
				if(liste[j]< liste[min]) {
					min = j; 
				}
			}
			
			if(min!=i) {
				tausch(liste,i,min);
			}
		}
	}
	
	

}
