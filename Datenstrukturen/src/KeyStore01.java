
public class KeyStore01 {
	
	private String[] key; 
	private int currentPos;
	public final int MAXPOSITION = 100; 
	public final int NICHTGEFUNDEN = -1;
	
	
	public KeyStore01() {
		this.key= new String[MAXPOSITION];
		this.currentPos = 0; 
	}
	
	public KeyStore01(int length) {
		this.key= new String[length];
		this.currentPos = 0;
	}
	
	
	public boolean add(String eintrag) {
		if(currentPos < MAXPOSITION) {
			this.key[this.currentPos] = eintrag;
			this.currentPos++;
			return true;
		}else {
			return false;
		}
	}
	
	public int indexOf(String eintrag) {
		//lineare Suche
		for(int i = 0; i< this.currentPos; i++) {
			if(this.key[i].equals(eintrag)) {
				return i;
			}
		}
		return NICHTGEFUNDEN;
	}
	
	public void remove(int index) {
		if(index>= 0 && index < currentPos) {
			for(int i = index; i < currentPos; i++) {
				this.key[i] = this.key[i +1];
			}
			currentPos--;
		}
	}

}
