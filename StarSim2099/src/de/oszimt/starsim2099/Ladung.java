package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Ladung {

	// Attribute
	int masse;
	String typ;
	double posX;
	double posY;
	

	// Methoden
	Ladung(){
		
	}
	
	public void setTyp(String typ) {
		this.typ = typ;
	}
	
	public String getTyp() {
		return typ;
	}
	
	public void setMasse(int masse) {
		this.masse = masse;
	}
	
	public int getMasse() {
		return masse; 
	}
	
	public void setposX(double posX) {
		this.posX = posX;
	}
	
	public double getposX() {
		return posX;
	}
	
	public void setposY(double posY) {
		this.posY = posY;
	}
	
	public double getposY() {
		return posY;
	}
	
	
	

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}