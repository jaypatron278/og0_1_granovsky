package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff {

	// Attribute
	double posX;
	double posY; 
	int maxKapazitaet; 
	String typ; 
	String antrieb; 
	int winkel; 
	
	// Methoden
	Raumschiff(){
		
	}
	
	public void setPosX(double posX) {
		this.posX = posX;
	}
	
	public double getPosX() {
		return posX;
	}
	
	public void setPosY(double posY) {
		this.posY = posY;
	}
	
	public double getPosY() {
		return posY;
	}
	
	public void setMaxLadekapazitaet(int maxKapazitaet) {
		this.maxKapazitaet = maxKapazitaet; 
	}
	
	public int getMaxLadekapazitaet() {
		return maxKapazitaet; 
	}
	
	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}
	
	public String getAntrieb() {
		return antrieb; 
	}
	
	public void setWinkel(int winkel) {
		this.winkel = winkel; 
	}
	
	public int getWinkel() {
		return winkel; 
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}

}
