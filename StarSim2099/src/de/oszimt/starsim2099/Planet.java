package de.oszimt.starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Planet {

	// Attribute
	double posX;
	double posY; 
	int anzahlHafen;
	String name; 
	
	// Methoden
	Planet(){

		
	}
	
	public void setPosX(double posX) {
		this.posX = posX;
	}
	
	public double getPosX() {
		return posX;
	}
	
	public void setPosY(double posY) {
		this.posY = posY;
	}
	
	public double getPosY() {
		return posY;
	}
	
	public void setAnzahlHafen(int anzahlHafen) {
		this.anzahlHafen = anzahlHafen; 
	}
	public int getAnzahlHafen() {
		return anzahlHafen;
	}
	
	public void setName(String name) {
		this.name = name; 
	}
	public String getName() {
		return name;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
