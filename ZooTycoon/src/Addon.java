import org.omg.Messaging.SyncScopeHelper;

public class Addon {
	
	int IdNummer; 
	String bezeichnung; 
	double verkaufsPreis; 
	int maxKapazitatBestand; 
	
	Addon(){
		
	}
	
	public void setIdNummer(int nummer) {
		this.IdNummer = nummer;
	}
	
	public int getIdNummer() {
		return IdNummer;
	}
	
	public void setbezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	public String getbezeichnung() {
		return bezeichnung;
	}
	
	public void setverkaufsPreis(double verkaufsPreis) {
		this.verkaufsPreis = verkaufsPreis; 
	}
	
	public double getverkaufsPreis() {
		return verkaufsPreis; 
	}
	
	public void setmaxKapazitatBestand(int kapazitat) {
		this.maxKapazitatBestand = kapazitat;
	}
	
	public int getmaxKapazitatBestand() {
		return maxKapazitatBestand;
	}
	
	public String addOnKaufen() {
		
		if(this.getmaxKapazitatBestand() < 12) {
			return "Sie haben diesen Addon gekauft> " + this.getbezeichnung();
		}else {
			return "Sie k�nnen diesen Artikel nicht kaufen, da sie keinen Bestand mehr haben" + this.getbezeichnung();
		}
		
	
	}
	
	

}
