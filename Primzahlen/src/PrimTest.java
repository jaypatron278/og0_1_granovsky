import java.util.Scanner;

public class PrimTest {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		long eingabe;
		System.out.println("Geben Sie eine Zahl ein");
		eingabe = scanner.nextLong();

		Primzahlen primzahlen = new Primzahlen();
		int zahler = 0;  
		long c = 0;
		while(zahler < 6) {
			long zeitStart = System.currentTimeMillis();
			System.out.println(Primzahlen.primzahlen(eingabe));
			long zeitEnde = System.currentTimeMillis();
			System.out.println("Die Zeit betr�gt " + (zeitEnde - zeitStart) + " Millisekunden ");
			zahler ++;
			c = (zeitEnde - zeitStart) / 6; 
		}
		System.out.println("Die genaue Zeit betr�gt: " + c);
		
		

	}

}
